from typing import Union

from loguru import logger
from openpyxl.cell import Cell
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet


class WsNotInWbError(BaseException):
    """Worksheet is not in workbook."""


class CellFactory:
    def __init__(self, wb: Workbook, ws: Worksheet):
        if ws not in wb.worksheets:
            logger.error(f"Worksheet is out of the workbook.")
            raise WsNotInWbError
        self._wb = wb
        self._ws = ws

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._wb)}, {repr(self._ws)})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {str(self._wb)}, {str(self._ws)}"

    @property
    def wb(self) -> Workbook:
        return self._wb

    @wb.setter
    def wb(self, value):
        self._wb = value

    @property
    def ws(self) -> Worksheet:
        return self._ws

    @ws.setter
    def ws(self, value):
        self._ws = value

    def cell_from_string(self, coord: str) -> Cell:
        return self._ws[coord]

    def cell_from_tuple(self, row: int, column: Union[str, int]) -> Cell:
        if isinstance(column, int):
            return self._ws.cell(row, column)
        else:
            return self._ws[f"{column}{row}"]
