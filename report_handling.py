from pathlib import Path

from cell_factory import CellFactory
from xlsx_file import XlsxFile
from youtrack_parser import Issue, RequestType, ResponseParser
from youtrack_request import CustomHTTPClient, CustomHTTPRequest, CustomPreparedRequest, issues_request, set_default


def main():
    set_default()

    path: Path = Path(__file__)
    xlsx_file: XlsxFile = XlsxFile(path)
    cell_factory: CellFactory = CellFactory(xlsx_file.wb, xlsx_file.ws)
    headers: list[str] = xlsx_file.headers()
    id_issues: list[str] = xlsx_file.get_column_header("ID задачи")

    issues_custom_request: CustomHTTPRequest = issues_request(id_issues)
    issues_custom_prepared_request: CustomPreparedRequest
    issues_custom_prepared_request = CustomPreparedRequest.from_custom_request(issues_custom_request)
    custom_http_client: CustomHTTPClient = CustomHTTPClient(issues_custom_prepared_request)
    response: dict = custom_http_client.get_response()
    issues_parser: ResponseParser = ResponseParser(response, RequestType.ISSUE)
    issues: list[Issue] = issues_parser.parse()


if __name__ == '__main__':
    print(add("check", "yes"))
