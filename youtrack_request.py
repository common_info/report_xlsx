import urllib.request
from enum import Enum
from typing import Iterable, Literal, NamedTuple, Optional, Union
from urllib.request import Request
from dotenv import dotenv_values
from loguru import logger

EntityType = Literal["Issue", "IssueWorkItem"]
ParsedResponse = Union[list['Issue'], list['IssueWorkItem'], dict[str, str]]

_config: dict[str, str] = dotenv_values(".env")
_auth_token: str = _config.get("AUTH_TOKEN")
_protocol = _config.get("PROTOCOL")

_headers_request: dict[str, str] = {
    "Authorization": " ".join(("Bearer", _auth_token)),
    "Accept": "application/json",
    "Content-Type": "application/json",
}


class RequiredAttributeMissing(BaseException):
    """Required attribute has no specified value."""


def set_default(is_secure: bool = True):
    if is_secure:
        globals()["_protocol"] = "HTTPS"
    else:
        globals()["_protocol"] = "HTTP"


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomHTTPRequest:
    def __init__(
            self, *,
            scheme: str = None,
            web_hook: str = None,
            host: str = None,
            port: int = None,
            params: tuple[tuple[str, str], ...] = None):
        if scheme is None:
            scheme = CustomScheme[f"{_protocol}"].value
        if port is None:
            port = CustomPort[f"{_protocol}"].value
        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params: tuple[tuple[str, str], ...] = params

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self.scheme}, web hook = {self.web_hook}, host = {self.host}, " \
               f"post = {self.port}, params = {self.params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.scheme}, {self.web_hook}, {self.host}, {self.port}, {self.params})>"

    def __hash__(self):
        return hash(self.url())

    @classmethod
    def __call__(cls):
        return cls()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()
        else:
            return NotImplemented

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def scheme(self):
        return self._scheme

    @scheme.setter
    def scheme(self, value):
        self._scheme = value

    @property
    def web_hook(self):
        return self._web_hook

    @web_hook.setter
    def web_hook(self, value):
        self._web_hook = value

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        self._params = value

    def _get_params(self):
        if self.params is None:
            return ""
        _params: str = "&".join(map(lambda x, y: f"{x}={y}", ((x, y) for item in self.params for x, y in item)))
        return f"?{_params}"

    def _get_web_hook(self):
        if self.web_hook is None:
            logger.error("Web hook is a required attribute")
            raise RequiredAttributeMissing
        return self.web_hook.rstrip("/")

    def _get_scheme(self):
        if self.scheme is None:
            return "https"
        return self.scheme.lower().strip("/")

    def _get_port(self):
        if self.port is None:
            return CustomPort[f"{_protocol}"].value
        return self.port

    def _get_host(self):
        if self.host is None:
            logger.error("Host is a required attribute")
            raise RequiredAttributeMissing
        return self.host.strip("/")

    def url(self):
        return f"{self._get_scheme()}://{self._get_host()}:{self._get_port()}{self.web_hook}{self._get_params()}"


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str

    def __str__(self):
        return f"{self.__class__.__name__}: method = {self.method}, host = {self.host}\n" \
               f"headers = {[header for header in self.headers]}\n" \
               f"url = {self.url}\n" \
               f"data = \"{self.data.decode('utf-8')}\""

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.url}, {self.data}, {[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() == other.to_request()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() != other.to_request()
        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None):
        if headers is None:
            headers = dict()
        if data is None:
            data = b''
        return cls(
            custom_http_request.url(),
            data,
            headers,
            custom_http_request.host,
            False,
            method)

    def to_request(self):
        if self.headers is None:
            self.headers = dict()
        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)


class CustomHTTPClient:
    def __init__(self, custom_prepared_request: CustomPreparedRequest):
        self._custom_prepared_request: CustomPreparedRequest = custom_prepared_request
        self._request: Optional[Request] = self.custom_prepared_request.to_request()
        logger.info(self.custom_prepared_request)

    def __str__(self):
        return f"{self.__class__.__name__}: custom http request = {self._custom_prepared_request}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._custom_prepared_request})>"

    @classmethod
    def __call__(cls):
        return cls(CustomPreparedRequest.from_custom_request(CustomHTTPRequest()))

    @property
    def request(self):
        return self._request

    @request.setter
    def request(self, value):
        self._request = value

    @property
    def custom_prepared_request(self):
        return self._custom_prepared_request

    @custom_prepared_request.setter
    def custom_prepared_request(self, value):
        self._custom_prepared_request = value

    def get_response(self):
        with urllib.request.urlopen(self.request) as req:
            return req.read().decode("utf-8")


def issues_request(issues: Iterable[str] = None):
    if issues is None:
        return
    scheme: str = CustomScheme(_protocol).value
    host: str = "youtrack.protei.ru"
    port: int = CustomPort(_protocol).value
    web_hook: str = "api/issues"

    string_issues: str = ",".join(issues)
    parameters_query: str = f'issue ID: {string_issues}'
    params: tuple[tuple[str, str], ...] = (
        ("fields", 'idReadable,customFields(value(name),projectCustomField(field(name)))'),
        ("query", parameters_query),
        ("customFields", "State")
    )
    return CustomHTTPRequest(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)


def issue_work_items_request(login: str, issues: Iterable[str] = None):
    if issues is None:
        return
    scheme: str = CustomScheme(_protocol).value
    port: int = CustomPort(_protocol).value
    host: str = "youtrack.protei.ru"
    web_hook: str = "api/workItems"

    string_issues: str = ",".join(issues)
    parameters_query: str = f'issue ID: {string_issues}'
    params: tuple[tuple[str, str], ...] = (
        ("fields", "duration(minutes),date,issue(idReadable)"),
        ("query", parameters_query),
        ("author", login)
    )
    return CustomHTTPRequest(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)
