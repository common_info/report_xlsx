from datetime import date
import re
from pathlib import Path
from re import Pattern, Match
from typing import Optional, Union

from loguru import logger
from openpyxl.utils.cell import get_column_letter

from xlsx_table import XlsxTable

PathLike = Union[str, Path]


def convert_to_date(date_string: str) -> date:
    day: int = int(date_string[:2], 10)
    month: int = int(date_string[3:5], 10)
    year: int = int(date_string[7:], 10)
    if 9 < year < 100:
        year += 2000
    return date(year, month, day)


class XlsxTableElement:
    def __init__(self, path: PathLike, index: int):
        self._xlsx_table: XlsxTable = XlsxTable(path, 0)
        self._index: int = index
        self._values: Optional[list[str]] = None

    def __str__(self):
        return f"{self.__class__.__name__}: {self._index}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._xlsx_table.path}{self._index})>"

    def __hash__(self):
        return hash((self._xlsx_table.path, self._index))

    def __key(self):
        return self._xlsx_table.path, self._index

    def __len__(self):
        return len(self._values)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self._xlsx_table.path == other._xlsx_table.path:
            return self._index > other._index
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__) and self._xlsx_table.path == other._xlsx_table.path:
            return self._index >= other._index
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self._xlsx_table.path == other._xlsx_table.path:
            return self._index < other._index
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__) and self._xlsx_table.path == other._xlsx_table.path:
            return self._index <= other._index
        else:
            return NotImplemented

    def __getitem__(self, item):
        return self._values.__getitem__(item)

    def __setitem__(self, key, value):
        self._values.__setitem__(key, value)

    @property
    def xlsx_table(self):
        return self._xlsx_table

    @xlsx_table.setter
    def xlsx_table(self, value):
        self._xlsx_table = value

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        self._index = value

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, value):
        self._values = value

    def cell(self, *args):
        raise NotImplementedError

    def __iter__(self):
        return iter(self._values)

    def __contains__(self, item):
        return item in self._values

    @property
    def heading(self) -> str:
        raise NotImplementedError

    def synchro(self):
        raise NotImplementedError


class XlsxRow(XlsxTableElement):
    def __init__(self, path: PathLike, index: int):
        super().__init__(path, index)
        self._values: Optional[list[str]] = self._xlsx_table.get_row(index)

    def cell(self, column: Union[str, int]):
        return self._xlsx_table.cell_factory.cell_from_tuple(self._index, column)

    def synchro(self):
        for column_index in range(len(self)):
            self.cell(column_index).value = self[column_index]

    @property
    def heading(self):
        return self.cell("A")


class XlsxColumn(XlsxTableElement):
    def __init__(self, path: PathLike, index: int):
        super().__init__(path, index)
        self._values: Optional[list[str]] = self._xlsx_table.get_row(index)

    @property
    def column_letter(self):
        return get_column_letter(self._index)

    def cell(self, column: Union[str, int]):
        return self._xlsx_table.cell_factory.cell_from_tuple(self._index, column)

    def synchro(self):
        for column_index in range(len(self)):
            self.cell(column_index).value = self[column_index]

    @property
    def heading(self):
        return self.cell(1)

    def is_heading(self, text: str):
        return self.heading.find(text) != -1

    def date_from_heading(self):
        date_pattern: Pattern = re.compile(r".*(?P<date_heading>\d{2}.\d{2}.\d{2,4}).*")
        match: Match = re.match(date_pattern, self.heading)
        if match:
            return convert_to_date(match.group("date_heading"))
        else:
            logger.error(f"В {self.heading} нет даты")
            raise ValueError

    def is_expired(self):
        return self.date_from_heading() > date.today()
