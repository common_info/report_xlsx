from enum import Enum
from os import environ
from pathlib import Path
from sys import platform
from typing import Optional, Mapping, Sequence, Iterable

import yaml
from loguru import logger
from yaml import YAMLError


class States(Enum):
    NEW = "New"
    ACTIVE = "Active"
    PAUSED = "Paused"
    DISCUSS = "Discuss"
    DONE = "Done"
    REVIEW = "Review"
    TEST = "Test"
    VERIFIED = "Verified"
    CLOSED = "Closed"

    def __missing__(self, key):
        return None


class ConfigurationManagerSetterError(BaseException):
    """Configuration Manager instance cannot be changed or modified."""


class ConfigurationManager:
    def __init__(self):
        self._yaml_file: Path = Path(__file__).with_name("logins_conversion.yaml")
        self._login: Optional[str] = None
        self._content: dict[str, dict[str, str]] = dict()

    def __repr__(self):
        return f"<{self.__class__.__name__}()>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._login}"

    def __setitem__(self, key, value):
        if isinstance(key, str) and isinstance(value, Mapping):
            self._content.__setitem__(key, {**value})
        else:
            raise KeyError

    @property
    def yaml_file(self):
        return self._yaml_file

    @property
    def login(self):
        return self._login

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    def read_config(self):
        try:
            with open(self._yaml_file, "r+") as f:
                content: dict[str, dict[str, str]] = yaml.safe_load(f)
        except PermissionError and IsADirectoryError and FileNotFoundError as e:
            logger.error(f"{e.__class__.__name__}, {e.strerror}")
        except RuntimeError and YAMLError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, {e.strerror}")
        else:
            self._content = content
            return self

    def write(self):
        with open(self._yaml_file, "w") as f:
            yaml.dump(self._content, f)

    def check_section(self, name: str):
        return name in self._content.keys()

    def logins_convert(self) -> dict[str, str]:
        return self._content.get("logins")

    def set_login(self):
        if platform.lower() == "win32":
            _login: str = environ.get("USERNAME")
        else:
            _login: str = environ.get("USER")
        self._login = self.logins_convert().get(_login, _login)
        return self


class ConfigurationSection:
    def __init__(
            self,
            name: str,
            configuration_manager: ConfigurationManager,
            items: Mapping[str, str] = None,
            required: Iterable[str] = None):
        if items is None:
            items: dict[str, str] = configuration_manager.content.get(name)
        if required is None:
            required: list[str] = []
        self._name: str = name
        self._configuration_manager: ConfigurationManager = configuration_manager
        self._items: dict[str, str] = {**items}
        self._required = list(filter(lambda x: x in items.keys(), required))

    def validate(self):
        return self._configuration_manager.check_section(self._name)

    @classmethod
    def from_configuration_manager(cls, name: str, configuration_manager: ConfigurationManager):
        return cls(name, configuration_manager, configuration_manager.content.get(name))

    @classmethod
    def from_base(cls, name: str):
        configuration_manager: ConfigurationManager = ConfigurationManager().read_config().set_login()
        return cls(name, configuration_manager, configuration_manager.content.get(name))

    @property
    def required(self):
        return self._required

    @required.setter
    def required(self, value):
        self._required = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, value):
        self._items = value

    @property
    def configuration_manager(self):
        return self._configuration_manager

    @configuration_manager.setter
    def configuration_manager(self, value):
        logger.error(f"ConfigurationManager is a singleton, so it cannot be replaced by {value}")
        raise ConfigurationManagerSetterError

    def __getitem__(self, item):
        return self._items.get(item)

    def __setitem__(self, key, value):
        self._items.__setitem__(key, value)
        logger.info(f"Пара {key} -> {value} добавлена")

    def __delitem__(self, key):
        self._items.__delitem__(key)
        logger.info(f"Значение {key} удалено")

    def synchro(self):
        self._configuration_manager.__setitem__(self._name, self._items)

    def __add__(self, other):
        if isinstance(other, Mapping):
            for k, v in other.items():
                self.__setitem__(k, v)
        else:
            return NotImplemented

    __radd__ = __add__

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._name == other._name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._name != other._name
        else:
            return NotImplemented

    def __iter__(self):
        return iter(self._items)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._items.keys()
        elif isinstance(item, Sequence) and len(item) == 2:
            return self._items.get(item[0]) == item[1]
        else:
            return False

    def all_items(self) -> str:
        return "\n".join(map(lambda x: f"{x[0]} -> {x[1]}", self._items.items()))

    def delete_all(self):
        for key in self._items:
            self.__delitem__(key)
        logger.info("Все значения удалены.")


class ConfigurationLogins(ConfigurationSection):
    def __init__(self, configuration_manager: ConfigurationManager):
        super().__init__("logins", configuration_manager)


class ConfigurationStates(ConfigurationSection):
    def __init__(self, configuration_manager: ConfigurationManager):
        super().__init__("states", configuration_manager)

    def __setitem__(self, key, value):
        if States.__members__.get(key, None) is None:
            _values: str = ", ".join(States.__members__.values())
            logger.warning(f"{key} нельзя добавить в качестве статуса. Допустимые значения:\n{_values}")
            return
        self._items.__setitem__(key, value)
        logger.info(f"Для статуса {key} задан текст {value} добавлена")
        return

    def __delitem__(self, key):
        self._items.__setitem__(key, "")
        logger.info(f"Значение {key} удалено")


def main():
    configuration_manager: ConfigurationManager = ConfigurationManager().read_config().set_login()
    logins: ConfigurationSection = ConfigurationSection.from_configuration_manager("logins", configuration_manager)
    states: ConfigurationSection = ConfigurationSection.from_configuration_manager("states", configuration_manager)
    logger.info(logins.all_items())
    logger.info(states.all_items())


if __name__ == '__main__':
    main()
