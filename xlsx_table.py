from pathlib import Path
from typing import Union

from loguru import logger
from openpyxl.reader.excel import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet

from cell_factory import CellFactory


class HeaderNotFound(BaseException):
    """No header is found in the table."""


class XlsxTable:
    def __init__(self, path: Union[str, Path], sheet_index: int):
        if isinstance(path, str):
            path: Path = Path(path)
        self._path: Path = path
        self._sheet_index: int = sheet_index

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}"

    def __hash__(self):
        return hash((self._path, self._sheet_index))

    def __key(self):
        return self._path, self._sheet_index

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __len__(self):
        return self.row_number * self.column_number

    def __bool__(self):
        return len(self) > 0

    @property
    def wb(self) -> Workbook:
        return load_workbook(self._path)

    @property
    def ws(self) -> Worksheet:
        return self.wb.worksheets[self._sheet_index]

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def sheet_index(self):
        return self._sheet_index

    @sheet_index.setter
    def sheet_index(self, value):
        self._sheet_index = value

    @property
    def cell_factory(self) -> CellFactory:
        return CellFactory(self.wb, self.ws)

    @property
    def column_number(self) -> int:
        return self.ws.max_column

    @property
    def row_number(self) -> int:
        return self.ws.max_row

    def iter_rows(self):
        for row in range(self.row_number):
            return self.ws.iter_rows(row, row, self.ws.min_column, self.column_number)

    def iter_columns(self):
        for column in range(self.column_number):
            return self.ws.iter_cols(self.ws.min_row, self.row_number, column, column)

    def rows(self):
        return list(self.iter_rows())

    def columns(self):
        return list(self.iter_columns())

    def get_row(self, row_index: int):
        return self.rows().__getitem__(row_index)

    def get_column(self, column_index: int):
        return self.columns().__getitem__(column_index)

    def __iter__(self):
        for row in range(self.row_number):
            for column in range(self.column_number):
                yield self.ws.cell(row, column)

    def headers(self) -> list[str]:
        return [self.cell_factory.cell_from_tuple(1, column).value for column in range(self.column_number)]

    def save(self):
        return self.wb.save(self._path)

    def column_header_index(self,  header: str):
        if header not in self.headers():
            logger.error(f"{header} is not found")
            raise HeaderNotFound
        return self.headers().index(header)

    def get_column_header(self, header: str):
        return self.get_column(self.column_header_index(header))
