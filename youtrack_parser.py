from _decimal import Decimal, ROUND_CEILING
from enum import Enum
from typing import Any, NamedTuple, Optional, Union

from loguru import logger
from numpy.core import datetime64
from numpy.core.umath import divide, isnat


def convert_long_date(long: Any) -> datetime64:
    """
    Convert the long value to the datetime64.

    :param long: the timestamp
    :return: the date associated with the timestamp.
    :rtype: datetime64
    """
    return datetime64(long, "ms").astype("datetime64[D]") if long else datetime64("NaT")


class RequestTypeNotSpecified(ValueError):
    """Request type cannot be equal to None."""


class RequestType(Enum):
    ISSUE = "issue"
    ISSUE_WORK_ITEM = "issue_work_item"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.value}>"


class Issue(NamedTuple):
    """
    Define the Issue entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        state --- the issue state, state;\n
        summary -- the issue short description, summary;\n
        parent --- the parent issue name, parent;\n
        deadline --- the issue deadline, deadline;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    state: str
    deadline: datetime64 = datetime64("NaT")

    def __str__(self):
        if isnat(self.deadline):
            deadline_string = ""
        else:
            deadline_string = f", deadline = {self.deadline.item().isoformat()}"
        return f"Issue: issue = {self.issue}, state = {self.state}, {deadline_string}"

    def __repr__(self):
        return f"<Issue(issue={self.issue}, state={self.state}, deadline={self.deadline})>"


class IssueWorkItem(NamedTuple):
    """
    Define the IssueWorkItem entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        date --- the issue work item date, datetime64;\n
        spent_time -- the issue work item recorded time in minutes, int;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    date: datetime64
    spent_time: Decimal

    def __str__(self):
        return f"IssueWorkItem: issue = {self.issue}, date = {self.date.item().isoformat()}, " \
               f"spent time = {self.spent_time}"

    def __repr__(self):
        return f"<IssueWorkItem(issue={self.issue}, date={self.date}, spent_time={self.spent_time})>"


class ResponseParser:
    _dict_parsers: dict[RequestType, type] = {
        RequestType.ISSUE: 'IssueResponseParser',
        RequestType.ISSUE_WORK_ITEM: 'IssueWorkItemResponseParser'
    }

    def __init__(self, response: Union[dict, list[dict]], request_type: Union[str, RequestType, None] = None):
        if isinstance(request_type, str):
            request_type: RequestType = RequestType[request_type]
        self._response: Union[dict, list[dict]] = response
        self._request_type: Optional[RequestType] = request_type

    def __str__(self):
        return f"{self.__class__.__name__}: {repr(self.request_type)})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.response.items()}, {repr(self.request_type)})>"

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        self._response = value

    @property
    def request_type(self):
        return self._request_type

    @request_type.setter
    def request_type(self, value):
        self._request_type = value

    @property
    def _child(self) -> Union['IssueResponseParser', 'IssueWorkItemResponseParser']:
        if self.request_type is None:
            logger.error("Request type is not specified")
            raise RequestTypeNotSpecified
        return self._dict_parsers[self.request_type](self.response, self.request_type)

    def parse(self):
        return self._child.parse()


class IssueResponseParser(ResponseParser):
    def parse(self) -> list[Issue]:
        issues: list[Issue] = []
        for response_item in self.response:
            issue: str = response_item["idReadable"]
            # specify the issue summary
            deadline: datetime64 = datetime64("NaT")
            state: str = ""
            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = convert_long_date(item["value"])
                else:
                    continue
            youtrack_issue: Issue = Issue(issue, state, deadline)
            issues.append(youtrack_issue)
        return issues


class IssueWorkItemResponseParser(ResponseParser):
    def parse(self) -> list[IssueWorkItem]:
        """
        Get the parameters from the response item.

        :return: the issue name, date, and modified spent time.
        :rtype: tuple[str, datetime64, Decimal, str]
        """
        issue_work_items: list[IssueWorkItem] = []
        for response_item in self.response:
            # define the issue name of the work item
            issue: str = response_item['issue']['idReadable']
            # define the date of the work item
            date: datetime64 = convert_long_date(response_item['date'])
            # define the spent time of the work item
            spent_time: int = response_item['duration']['minutes']
            # convert to the hours
            modified_spent_time: Decimal = Decimal(divide(spent_time, 60)).quantize(Decimal("1.00"), ROUND_CEILING)
            youtrack_issue_work_item: IssueWorkItem = IssueWorkItem(issue, date, modified_spent_time)
            issue_work_items.append(youtrack_issue_work_item)
        return issue_work_items
